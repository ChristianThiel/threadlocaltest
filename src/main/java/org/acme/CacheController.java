package org.acme;

import io.quarkus.cache.CacheResult;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

@RequestScoped
public class CacheController {

    @Inject
    ThreadLocalContext threadLocalContext;
    
    //second way to use threadLocal
    ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public String load(int id) {
        threadLocalContext.put("key", "value");
        threadLocal.set("Hallo");
        return loadIntern(id);
    }

    @CacheResult(cacheName = "myCache")
    public String loadIntern(int id) {
        //no values found in threadLocals...:(
        String hallo = threadLocal.get();
        System.out.println("org.acme.Controller.loadIntern() 1 " + hallo);
        Object value = threadLocalContext.get("key");
        System.out.println("org.acme.Controller.loadIntern() 2 " + value);
        return hallo+ " "+value + " " + id;
    }
}
