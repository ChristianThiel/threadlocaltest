package org.acme;

import java.util.HashMap;
import java.util.Map;
import javax.enterprise.context.RequestScoped;

@RequestScoped
public class ThreadLocalContext {

    private final ThreadLocal<Map<String, Object>> data = new ThreadLocal<>();

    public void put(String key, Object payload) {
        Map<String, Object> map = data.get();
        if (map == null) {
            map = new HashMap<>();
            data.set(map);
        }
        map.put(key, payload);
    }

    public Object get(String key) {
        Map<String, Object> map = data.get();
        return map != null
                ? map.get(key)
                : null;
    }

    public void cleanupThread() {
        data.remove();
    }

    public boolean containsKey(String key) {
        Map<String, Object> map = data.get();
        return map != null && map.containsKey(key);
    }

    public Object remove(String key) {
        Map<String, Object> map = data.get();
        if (map != null) {
            return map.remove(key);
        }
        return null;
    }

    @Override
    public String toString() {
        return "ThreadLocalContextHolder{" + data.get() + '}';
    }
}
