package org.acme;

import io.quarkus.test.junit.QuarkusTest;
import javax.inject.Inject;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class CacheControllerTest {

    @Inject
    CacheController testee;

    @Test
    public void shouldLoadWithContextPropagation() throws Exception {
        //given
        //when
        String result = testee.load(0);
        //then
        assertThat(result, is("Hallo value 0"));
    }
}
